#!/bin/bash

cat system/framework/oat/arm64/services.odex.* 2>/dev/null >> system/framework/oat/arm64/services.odex
rm -f system/framework/oat/arm64/services.odex.* 2>/dev/null
cat system/app/Chrome/Chrome.apk.* 2>/dev/null >> system/app/Chrome/Chrome.apk
rm -f system/app/Chrome/Chrome.apk.* 2>/dev/null
cat system/app/facebook/facebook.apk.* 2>/dev/null >> system/app/facebook/facebook.apk
rm -f system/app/facebook/facebook.apk.* 2>/dev/null
cat system/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> system/app/WebViewGoogle/WebViewGoogle.apk
rm -f system/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat system/app/SBrowser_5.0/SBrowser_5.0.apk.* 2>/dev/null >> system/app/SBrowser_5.0/SBrowser_5.0.apk
rm -f system/app/SBrowser_5.0/SBrowser_5.0.apk.* 2>/dev/null
cat system/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/priv-app/Velvet/Velvet.apk
rm -f system/priv-app/Velvet/Velvet.apk.* 2>/dev/null
