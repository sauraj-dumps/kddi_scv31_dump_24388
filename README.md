## zeroltekdi-user 7.0 NRD90M SCV31KDU1CQG1 release-keys
- Manufacturer: samsung
- Platform: exynos5
- Codename: SCV31
- Brand: KDDI
- Flavor: zeroltekdi-user
- Release Version: 7.0
- Id: NRD90M
- Incremental: SCV31KDU1CQG1
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: ja-JP
- Screen Density: 640
- Fingerprint: KDDI/SCV31_jp_kdi/SCV31:7.0/NRD90M/SCV31KDU1CQG1:user/release-keys
- OTA version: 
- Branch: zeroltekdi-user-7.0-NRD90M-SCV31KDU1CQG1-release-keys
- Repo: kddi_scv31_dump_24388


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
