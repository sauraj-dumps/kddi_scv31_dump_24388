#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/15570000.ufs/by-name/RECOVERY:30273536:f2206f591adcf4a754c548eacbf8e3897d3febc9; then
  applypatch EMMC:/dev/block/platform/15570000.ufs/by-name/BOOT:26159104:1f295c809c68fefc56f4a3f68161e5184447fe93 EMMC:/dev/block/platform/15570000.ufs/by-name/RECOVERY f2206f591adcf4a754c548eacbf8e3897d3febc9 30273536 1f295c809c68fefc56f4a3f68161e5184447fe93:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
